resource "aws_security_group" "SG" {
  name       = "Main Security Group"
  vpc_id     = var.vpc_id
  #depends_on = [aws_vpc.main]
  tags = {
    Name = "SG"
  }

  dynamic "ingress" {
    for_each = [80, 8080, 443, 4443, 22]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
