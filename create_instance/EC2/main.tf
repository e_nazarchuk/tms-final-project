resource "tls_private_key" diplom_key  {
  algorithm = "RSA"
}


resource "aws_key_pair" "diplom_key" {
  key_name    = "Diplom_project"
  public_key = tls_private_key.diplom_key.public_key_openssh
  }

  resource "local_file" "private_key" {
  depends_on = [
    tls_private_key.diplom_key,
  ]
  content  = tls_private_key.diplom_key.private_key_pem
  filename = "Diplom_project.pem"
}


resource "aws_instance" "dev-server" {
  ami           = data.aws_ami.ubuntu.id 
  instance_type = "t2.micro"
  #subnet_id = var.public_subnet_ids[1]
  subnet_id   = flatten(var.public_subnet_ids)[0]
  vpc_security_group_ids = [var.SG_VPC]

  key_name = "Diplom_project"

  tags = {
    Name = "dev-server"
    }
}

resource "aws_instance" "prod-server" {
  ami           = data.aws_ami.ubuntu.id 
  instance_type = "t2.micro"
  subnet_id   = flatten(var.public_subnet_ids)[1]
  vpc_security_group_ids = [var.SG_VPC]
  key_name = "Diplom_project"

  tags = {
    Name = "prod-server"
    }
}

resource "aws_instance" "monitoring-server" {
  ami           = data.aws_ami.ubuntu.id 
  instance_type = "t2.micro"
  subnet_id   = flatten(var.public_subnet_ids)[1]
  vpc_security_group_ids = [var.SG_VPC]
  key_name = "Diplom_project"

  tags = {
    Name = "monitoring-server"
    }
}

resource "aws_instance" "master-server" {
  ami           = data.aws_ami.ubuntu.id 
  instance_type = "t2.micro"
  subnet_id   = flatten(var.public_subnet_ids)[1]
  vpc_security_group_ids = [var.SG_VPC]
  key_name = "Diplom_project"

  tags = {
    Name = "master-server"
    }
}